title: "Explore postmarketOS"
---

### 🧠 Knowledge

* [**Blog**](/blog) &mdash;
  Release announcements and other news
* [**FAQ**](/faq) &mdash;
  Frequently Asked Questions for people new to the project
* [**Podcast**](https://cast.postmarketos.org/) &mdash;
  Long-form discussions and interviews with the wider Linux Mobile scene
* [**Wiki**](https://wiki.postmarketos.org/) &mdash;
  Lots of articles about porting, devices, UIs, mainlining and more

### 🚧 Development

* [**Edge Blog**](/edge) &mdash;
  Breakage notes for the development version of postmarketOS
* [**Source Code**](/source-code/) &mdash;
  We proudly license all of our work under free software licenses
* [**Package Search**](https://pkgs.postmarketos.org/packages) &mdash;
  Find package versions, dependencies and contents
* [**BPO**](https://build.postmarketos.org) &mdash;
  Look at the queue and logs of package and image builds

### 💬 Discussion Platforms

* [**Matrix/IRC**](https://wiki.postmarketos.org/wiki/Matrix_and_IRC) &mdash;
  Get help with using or developing postmarketOS in real time
* [**Mastodon**](https://fosstodon.org/@postmarketOS) &mdash;
  Mostly boosting cool postmarketOS ports and developments from around the world
* [**Lemmy**](https://lemmy.ml/c/postmarketos) &mdash;
  Threaded discussions on links and questions
* [**Events**](/events) &mdash;
  IRL conferences with postmarketOS developers, talks and demos

### 👕 Clothing

* [**Merch**](/merch) &mdash;
  Shirts with postmarketOS logos, a cat chewing on an N900 and what not
