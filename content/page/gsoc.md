title: "postmarketOS ideas for Google Summer of Code 2024"
---
This is the list of project ideas that the postmarketOS
[Core Team](https://wiki.postmarketos.org/wiki/Team_members#Core_Team) is
interested in mentoring. GSoC interns can also propose their very own project
ideas, but we expect them to engage with the community before or during
the application period to get feedback and guidance to improve the proposal. If
you are interested in proposing a project idea, make sure to have a
postmarketOS-capable [device](https://wiki.postmarketos.org/wiki/Devices) and
contact us on [Matrix](https://wiki.postmarketos.org/wiki/Matrix_and_IRC).
To apply for any of our proposals, having a device is not a requirement, as
they can be developed with QEMU. If the applicant lacks a device by the end
of the project, we'll surely be provide one!

# Proposals

## Upstreaming and improving [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox/)

For many years, postmarketOS has been maintaining a small set of CSS and JS
hacks to make desktop Firefox more useful on mobile. The project is widely used
outside postmarketOS. We would like to
[upstream](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/28)
as many of our changes to Firefox as possible. This will include addressing
feedback from Firefox maintainers and working with them to get these changes
accepted. We will provide help with the management and communication with
upstream developers.

**Requirements:**

CSS and JS experience. Nice to have would be experience debugging Firefox and
creating adaptive designs with CSS and JS.

**Communication:**

Matrix (video call and email are secondary comms)

**Mentors:** [Pablo Correa Gomez](https://gitlab.com/pabloyoyoista), [Oliver Smith](https://gitlab.com/ollieparanoid)

**Project length:** 90, 175, or 350 hours. Any help is welcomed, and there's
enough things to upstream to cover a 350 hours project.

## pbsplash improvements: IPC, rotation, animations

pbsplash is a tiny utility used in postmarketOS for displaying an animation
during boot. It is optimized for size, since it needs to be installed on some
devices with a very small boot partition, where existing programs would be too
large. As with any new tool, it is missing some features that would improve the
user experience considerably.

**Requirements:**

Good C language knowledge. Knowledge on Linux graphics would be helpful.

**Communication:**

Matrix (video call and email are secondary comms)

**Mentors:** [Clayton Craft](https://gitlab.com/craftyguy), [Caleb Connolly](https://gitlab.com/calebccff)

**Project length:** 175 or 350 hours.

## tqftpserv rewrite in Rust

[tqftpserv](https://github.com/andersson/tqftpserv) is a project developed and
used for various Qualcomm SoCs (phones, laptops, etc.) which implements a
non-standard TFTP server which is used to serve firmware requests from the
modem. The communication happens over a QRTR socket and is used by all modern
phones with Qualcomm SoC in postmarketOS.

The implementation is currently written in C. A new implementtion in Rust is
desirable to ensure memory safety and correctness, and at the same time making
it easier to develop tests to ensure that no regression happens in the future.

**Requirements:**

Good Rust and C language knowledge.

**Communication:**

Matrix (video call and email are secondary comms)

**Mentors:** [Pablo Correa Gomez](https://gitlab.com/pabloyoyoista), [Luca Weiss](https://gitlab.com/z3ntu)

**Project length:** 175 or 350 hours.
