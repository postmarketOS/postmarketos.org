title: "Events"
preview: "2024-08/fossy.jpg"
---

[![A table with a postmarketOS tablecloth, a laptop and phone running postmarketOS](/static/img/2024-08/fossy.jpg){: class="wfull" }](/static/img/2024-08/fossy.jpg)

This page lists various conferences where we have a stand, give talks or just
hang out. Meet us there if you want to talk to postmarketOS developers or try
it out on demo devices!

Join the [#events:postmarketos.org](https://matrix.to/#/#events:postmarketos.org)
matrix chat for discussing events.

## List of events

### 2024

* 2024-10-26:
  [Open Source Conference 2024 Tokyo/Fall](https://event.ospn.jp/osc2024-fall/)
  (Tokyo, JP)
* 2024-09-25 to 26:
  [All Systems Go!](https://all-systems-go.io/)
  (Berlin, DE)
* 2024-09-07 to 12:
  [Akademy 2024](https://akademy.kde.org/2024/)
  (Würzburg, DE)
* 2024-08-17 to 18:
  [FrOSCon 2024](https://froscon.org/)
  (St. Augustin, DE)
* 2024-08-01 to 04:
  [FOSSY 2024](https://2024.fossy.us/)
  (Portland, US)
* 2024-07-19 to 24:
  [GUADEC 2024](https://events.gnome.org/event/209/)
  (Denver, US / Berlin, DE)
* 2024-02-03 to 04:
  [FOSDEM 2024](https://fosdem.org/2024/)
  (Brussels, BE)

## FAQ

### Where can I learn more about what is happening at the events?

As events are happening, we usually post about it on
[Mastodon](https://fosstodon.org/@postmarketOS). Sometimes we also record a
podcast at or after events, e.g.
[#38 FOSDEM 2024 Special](https://cast.postmarketos.org/episode/38-FOSDEM-2024-special/).

### Can I represent postmarketOS at an event?

Yes if you are a Trusted Contributor or Core Contributor (see [team](/team)).
Otherwise please get in contact with the team, we want to make sure that there
is a common understanding of our culture, valuing collaboration over
competition, praising and not bashing other mobile linux distros, recognizing
value in upstreaming fixes, etc.

When representing postmarketOS at an event, please submit a patch to the
[postmarketos.org](https://gitlab.com/postmarketOS/postmarketos.org) repository
to add the event to this page.
