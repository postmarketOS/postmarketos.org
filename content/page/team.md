title: "Who makes postmarketOS?"
---

## Contributors

As a free and open-source software (FOSS) project, anyone can contribute to
postmarketOS by submitting patches and taking part in discussions.
There are a wide range of contributions that come from
all corners of our amazing community!
You too [can contribute](https://wiki.postmarketos.org/wiki/Contributing)!

All creatures welcome. Make sure to follow the [Code of Conduct](/coc).

### Contributor Groups

Members who have special responsibilities are organized into the following
groups.

#### Core Contributors

The Core Contributors are the most involved with the project. They set the
overall project direction, manage finances, design and maintain the
infrastructure, make releases, and everything else.

* [Who are the Core Contributors?](/core-contributors)

#### Trusted Contributors

Trusted Contributors work on day-to-day things across the project including
code reviews, merging changes, and issue triage.

* [Who are the Trusted Contributors?](/trusted-contributors)
* [How to become a Trusted Contributor?](/blog/2023/12/03/how-to-become-tc/)
  ([Part 2](/blog/2024/01/28/how-to-become-tc2/))

#### Active Contributors

Active Contributors can't merge changes themselves, but otherwise do much of
what Trusted Contributors do. They usually focus on the specific devices they're
interested in.

* [Who are the Active Contributors?](https://gitlab.com/groups/postmarketOS/active-community-members/-/group_members)
  &mdash; *Look for "Direct member" in the source column, for technical reasons
  this gitlab group contains Core Contributors and Trusted Contributors too.*
* [How to become an Active Contributor?](/blog/2024/05/14/active-community-members/)

### Teams

postmarketOS is also organized into Teams, which are built around a singular
topic or purpose. Anyone in the community can join a team, they're not limited
to any specific contributors. Right now we have only one, the Testing Team:

#### Testing Team

Given the breadth of hardware postmarketOS supports, extensive testing is
important to prevent regressions: the Testing Team tests changes before they're
released ensuring they're as reliable as possible.

* [Who is in the Testing Team / How to join?](https://wiki.postmarketos.org/wiki/Testing_Team)
