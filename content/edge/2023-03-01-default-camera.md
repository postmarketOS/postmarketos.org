title: "Changes to camera apps installation and packaging"
date: 2023-03-01
---

There have been several changes to how camera apps in postmarketOS are installed on devices:

- Device packages for devices with working cameras now have a subpackage,
`*-camera`, that is installed by default. E.g. `device-pine64-pinephone-camera`.
- `megapixels-gtk3` has been dropped from pmaports, this package was originally
created to support sxmo's X11/DWM variant, but it is no longer required.
`megapixels` should now work with this UI.
- `megapixels-purism` has been dropped from pmaports, the app was packaged in
Alpine Linux aports as `millipixels`
- pmOS images for devices that do **not** support a camera will no longer
include `megapixels` by default. Images for devices that **do** support a
camera will have the relevant camera app installed by default.
- `postmarketos-default-camera` is a soft dependency, it can be uninstalled if
having a camera app is not desired. Removing this package will remove any
supported camera app from the install.

## Action

Users with existing installs of pmOS on devices that support cameras can
install `postmarketos-default-camera` to have the relevant camera app for their
device installed:

```
$ sudo apk update
$ sudo apk add postmarketos-default-camera
```

Other users with devices that do *not* have working cameras can safely remove `megapixels` (with `sudo apk del megapixels`)

Also see:

  - [pma!3815 -  Allow device packages to set a default camera app ](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3815)
  - [aports!43318 - Add millipixels](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/43318)
  - [Camera subpackage description](https://wiki.postmarketos.org/wiki/Device_specific_package#Camera_subpackage)
