title: "Reinstall alsa-ucm-conf to restore audio on the PinePhone"
date: 2024-08-07
---

The PinePhone's device-pine64-pinephone package carries an ALSA UCM config
that overrides the one provided by upstream alsa-ucm-conf in
Alpine's alsa-ucm-conf package. The goal is to drop these overrides
completely, and {{MR|5462|pmaports}} makes some effort to reduce the diff
between our overrides and upstream. One of the overrides that have been
removed by that MR is a symlink - the device-pine64-pinephone package will
no longer install this symlink and it will be provided by
the Alpine alsa-ucm-conf package instead. However, due to a limitation of
the Alpine package manager, just upgrading the device-pine64-pinephone package
will delete the symlink and *not* restore it from the alsa-ucm-conf package,
which will cause audio to break. Users will need to manually reinstall
the Alpine alsa-ucm-conf package to restore working audio.

In other words:

1. Initially, with device-pine64-pinephone-3-r4 or earlier installed,
   the symlink `/usr/share/alsa/ucm2/conf.d/simple-card/PinePhone.conf`
   will point to `/usr/share/alsa/ucm2/PinePhone/PinePhone.conf`.

2. Install upgrades, which will install device-pine64-pinephone-4-r0 or later.
   The symlink will now be gone. The `/usr/share/alsa/ucm2/PinePhone` directory
   will also be gone. In this state, audio will stop working.

3. Reinstall the alsa-ucm-conf package by running
   `apk fix --reinstall alsa-ucm-conf`. The symlink will now exist again,
   now pointing to `/usr/share/alsa/ucm2/Allwinner/A64/PinePhone/PinePhone.conf`.

4. Log out and back in, or simply reboot, so that pipewire-pulse etc
   are restarted and pick up the new UCM config. Audio will now work again.
