title: "FDE broken on postmarketos-initramfs 3.2.0-r0"
date: 2024-06-27
---

{{MR|5048|pmaports}} was just recently merged and includes another set of 
improvements to the initramfs! However, it alsos introduced a bug which 
would make FDE setups unbootable.

If your device is affected by this bug, you may get the following message on 
bootup:

```
[   16.698654] [pmOS-rd]: Mount root partition (/dev/mmcblk1p2) to /sysroot (read-only) with options defaults
[   16.706644] [pmOS-rd]: Detected crypto_LUKS filesystem
[   16.707829] [pmOS-rd]: ERROR: Detected unsupported 'crypto_LUKS' filesystem (/dev/mmcblk1p2).
```

And `ERROR: unsupported 'crypto_LUKS' filesystem (/dev/mmcblk1p2)` on the splash 
screen.

If you have upgraded to `3.2.0`, **do not reboot** until `3.2.1` is 
available/installed.

If you already rebooted, you can recover the system by entering debug-shell, 
manually mounting boot and root partitions and `chroot` to your installation, 
and downgrading to `3.1.0-r2`.
This is how it looks like on a OnePlus 6 with pmOS installed on slot B, userdata:

```
# umount /boot
# mount /dev/mapper/root /sysroot
# mount /dev/mapper/userdata1 /sysroot/boot
# chroot /sysroot
# openrc sysinit
# apk add --allow-untrusted /var/cache/apk/postmarketos-initramfs-3.1.0-r2.547ef266.apk
# exit
# dd if=/sysroot/boot/boot.img of=/dev/disk/by-partlabel/boot_b
# reboot -f
```
