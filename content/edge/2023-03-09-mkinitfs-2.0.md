title: "Changes due to the release of mkinitfs 2.0"
date: 2023-03-09
---

A recent release of the tool for generating the initramfs archives has
introduced path (1) and format (2) changes that may affect users who have configured
their installation of postmarketOS to use custom initramfs hooks or other
files.

**Users who did not add custom hooks or files to the initramfs should not be
impacted by these changes.** If any issues are encountered, please [file an
issue in the pmaports repo](https://gitlab.com/postmarketOS/pmaports/-/issues)
so that we can take a look.

This new release of mkinitfs supports using more compression formats for the
initramfs, and simplifies how the initramfs and initramfs-extra archives are
defined and configured within the OS, while allowing experienced users to
continue adding their own customizations to these archives.

## 1. Path Changes

`mkinitfs` now searches different directories for configuration used to
generate the initramfs archives. A summary of the path changes are below. Hooks
or files added to the old path should be manually migrated to the new paths in
order for mkinitfs to include user changes in the initramfs archives:


| Old Path                                      | New Path                         | Path Purpose                                                                |
|-----------------------------------------------|----------------------------------|-----------------------------------------------------------------------------|
| `/etc/postmarketos-mkinitfs/hooks{,-extra}`   | `/etc/mkinitfs/hooks{,-extra}`   | Hooks/scripts to include                                                    |
| `/etc/postmarketos-mkinitfs/files{,-extra}`   | `/etc/mkinitfs/files{,-extra}`   | Lists of files to include                                                   |
| `/etc/postmarketos-mkinitfs/modules{,-extra}` | `/etc/mkinitfs/modules{,-extra}` | Lists of kernel modules to include                                          |
| `none`                                        | `/etc/mkinitfs/dirs`             | Lists of directories to include (Note: This feature is new to mkinitfs 2.0) |


## 2. Format Changes

The format of the file lists in the `files` path can now accept a source and
destination for each path listed. This allows placing a given file at a
different path in the initramfs. If no destination is specified, then the file
will be included in the initramfs at the same path as the original file.

For more information about these two changes, see `man 1 mkinitfs`.

Also see:

  - [pma!3895- Support (postmarketos-)mkinitfs 2.0 ](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3895)
  - [mkinitfs 2.0 Release](https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/tags/2.0)
