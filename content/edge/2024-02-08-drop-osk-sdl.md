title: "osk-sdl removed from pmaports"
date: 2024-02-08
---

The `osk-sdl` application has been dropped from pmaports and the postmarketOS
repo binary repository, and has been replaced by `unl0kr`. For many years
`osk-sdl` was the default application that enabled unlocking full disk
encryption on boot in postmarketOS. It was marked as deprecated in October
2023.

Please pay special attention when upgrading if you have full disk encryption
enabled and have not migrated to `unl0kr` yet, osk-sdl will be removed and
`unl0kr` will be installed. If this does not happen, please reach out to us on
[chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC) or file an
[issue](https://gitlab.com/postmarketOS/pmaports/-/issues) and **do not** reboot.

Related: {{MR|4732|pmaports}}
