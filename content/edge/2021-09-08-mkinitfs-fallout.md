title: "Fallout from new mkinitfs (OnePlus 6/6T, 8T, devices using Alpine kernels like the Raspberry Pi)"
date: 2021-09-07
---

Regression from deploying [new mkinitfs](/edge/2021/09/03/new-mkinitfs/), the
OnePlus 6/6T and 8T devices using kernels from Alpine run into errors while
upgrading.

Binary packages with fixes should be available shortly.

### Affected devices

  * cubietech-cubieboard
  * oneplus-enchilada
  * oneplus-fajita
  * oneplus-kebap
  * raspberry-pi*
  * surftab-wintron7.0
  * tablet-x64uefi
  * tablet-x86uefi
  * trekstor-surftabduow1
  * xiaomi-beryllium
  * xiaomi-equuleus

### Fixes

oneplus-enchilada, oneplus-fajita:

  * linux-postmarketos-qcom-smd845 5.14.0_rc6-r1
  * boot-deploy 0.2

oneplus-kebap:

  * linux-oneplus-kebab 4.19.110-r2

all other devices:

  * postmarketos-mkinitfs 1.0.2-r1

### Possible errors

/boot/vmlinuz-* getting removed, resulting in:

```
Executing postmarketos-mkinitfs-1.0.2-r0.trigger
2021/09/08 19:36:53 Generating for kernel version: 5.14.0-rc6-sdm845
2021/09/08 19:36:53 Output directory: /boot
2021/09/08 19:36:53 == Generating initramfs ==
2021/09/08 19:36:53 - Including hook files
2021/09/08 19:36:53 - Including hook scripts
2021/09/08 19:36:53 - Including required binaries
2021/09/08 19:36:53 - Including kernel modules
2021/09/08 19:36:53 - Including splash images
2021/09/08 19:36:53 - Writing and verifying initramfs archive
2021/09/08 19:36:53 == Generating initramfs extra ==
2021/09/08 19:36:53 - Including extra binaries
2021/09/08 19:36:53 - *NOT* including FDE support
2021/09/08 19:36:53 - Writing and verifying initramfs-extra archive
2021/09/08 19:36:53 == Using boot-deploy to finalize/install files ==
2021/09/08 19:36:53 Unable to find any kernels at /boot/vmlinuz*
ERROR: postmarketos-mkinitfs-1.0.2-r0.trigger: script exited with error 1
```

OnePlus 6/6T: trying to flash boot.img before it was created with
boot-deploy 0.1:

```
2021/09/08 20:32:20 == Using boot-deploy to finalize/install files ==
==> kernel: device-tree blob operations
==> kernel: appending device-tree qcom/sdm845-oneplus-enchilada
==> initramfs: creating boot.img
==> Flashing boot image
Flashing boot.img to 'boot_b'
dd: can't open '/boot/boot.img': No such file or directory
2021/09/08 20:32:20 'boot-deploy' command failed:
2021/09/08 20:32:20 exit status 1
```

### Related MRs

* [pma!2499](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2499)
* [pma!2500](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2500)
* [pma!2501](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2501)
