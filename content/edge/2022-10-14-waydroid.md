title: "Waydroid: pmOS image deleted, requires user intervention"
date: 2022-10-14
---

We used to have a waydroid image in postmarketOS, but it was outdated and
broke waydroid
([pma#1653](https://gitlab.com/postmarketOS/pmaports/-/issues/1653)). It was
decided to remove the waydroid image from the postmarketOS repositories and
just using the image provided by upstream
([pma!3508](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3508)).

People who use Waydroid on postmarketOS edge now need to do the following:

* Upgrade as usually with `sudo apk upgrade -a` (or e.g. via GNOME Software,
  which does that internally). This will remove the waydroid-image package.
* Run `sudo waydroid init` to get the upstream image.
