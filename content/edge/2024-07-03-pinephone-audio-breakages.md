title: "Various audio breakages on the PinePhone"
date: 2024-07-03
---

Various issues have come together to break audio on the PinePhone.

First, for all DEs that use pulseaudio for audio, users will not have
any audio. This is because pulseaudio keeps crashing with:

>[pulseaudio] alsa-ucm.c: Assertion 'dev == data->device' failed at ../src/modules/alsa/alsa-ucm.c:1562, function pa_alsa_ucm_set_port(). Aborting.

This issue happened with the pulseaudio v17 upgrade. It will be fixed by
[this aports MR.](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/68586)

Next, for DEs that use pipewire-pulse for audio, users will have audio
from regular applications, but audio during a call will not work.
This is because of the pipewire v1.2.0 upgrade which changed how the sound card
exposes audio profiles. The issue is that callaudiod does not understand these new profiles,
so it does not switch to the Voice Call profile when a call is connected.
Audio playback outside of a call is unaffected.

Note that once the pulseaudio crash is fixed by the above aports MR,
the fixed pulseaudio will also generate the same new profiles like pipewire v1.2.0 does,
which callaudiod will not understand. So simply using the pulseaudio packages from
that aports MR's CI will unfortunately not help with functional call audio.

The issue is being discussed in {{issue|2968|pmaports}}

If you urgently need audio / call audio to work, the most straightforward workaround
is to switch to v24.06. The pulseaudio package in v24.06 is v16
that doesn't crash and doesn't generate the new profiles that break callaudiod.
The pipewire-pulse package also doesn't generate the new profiles
that break callaudiod.

Additional workarounds are listed below, but note that these workarounds are more advanced,
so do not perform them if you don't understand them.

- If you're on pulseaudio audio, first switch to pipewire audio by replacing
  the `postmarketos-base-ui-audio-pulseaudio` package with
  the `postmarketos-base-ui-audio-pipewire` package. Then disable callaudiod,
  and switch audio profiles yourself with `pactl set-card-profile` when connecting
  or disconnecting a call. SXMO users may find this easy via
  the `sxmo_hook_pickup.sh` and `sxmo_hook_hangup.sh` scripts.

- If you're already on pipewire audio and you still have the old APKs of
  pipewire-1.0.6-r6 in `/var/cache/apk`, downgrade all pipewire packages to 1.0.6-r6.
  If you do this and notice wireplumber crashing with:

  >Error relocating /usr/lib/libwireplumber-0.5.so.0: pw_log_set_level_string: symbol not found

  ... then you will also need to downgrade wireplumber to 0.5.3,
  otherwise you will not have any audio at all.

- If you're on pulseaudio audio, first switch to pipewire audio by replacing
  the `postmarketos-base-ui-audio-pulseaudio` package with
  the `postmarketos-base-ui-audio-pipewire` package. Then install
  the custom callaudiod script in [this comment.](https://gitlab.com/postmarketOS/pmaports/-/issues/2968#note_1977213976)
  This script replaces the default callaudiod with a reimplementation that supports
  the new profiles.
