title: "Python 3.10 upgrade in progress -- python apps may fail to start"
date: 2021-12-17
---

Python in Alpine Linux aports was recently upgraded to 3.10, and all related
packages that depend on python need to be rebuilt. This may take some time for
the Alpine Linux builders to work through.

If there are any applications that continue to fail after the rebuilds are
done, it's possible they were missed and not included in the rebuild, so please
file an issue in the Alpine aports repo or postmarketOS pmaports repo
(depending on where the package is found) or, ideally, send a patch to have it
rebuilt to the relevant repo.
