title: "phoc upgrade file conflicts with wlroots-phosh"
date: 2022-03-30
---

A recent update to the `phoc` package in Alpine aports may result in an error
when installing the upgrade:

```
...
(15/16) Upgrading phoc (0.13.0-r0 -> 0.13.0-r1)
ERROR: phoc-0.13.0-r1: trying to overwrite usr/lib/libwlroots.so.9 owned by wlroots-phosh-0.14.1-r0.
(16/16) Purging wlroots-phosh (0.14.1-r0)
```

If you encounter this, it can be resolved by simply running:

```
$ sudo apk fix
```

The `phoc` package will be fixed in aports soon. 

### Also see:

- [aports!32660](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/32660)
