title: "Generic x86_64 OEM-specific packages need manual reinstall"
date: 2024-01-09
---

A recent update to the Generic x86_64 device package
(`device-generic-x86_64-9-r0`) will uninstall OEM-specific packages, and manual
intervention is required to reinstall the packages that are appropriate for
your device:

* For users with AMD-based systems: `apk add oem-amd`
* For users with Intel-based systems: `apk add oem-intel`

When building your own `generic-x86_64` image with pmbootstrap, the packages
will be automatically added with the `_pmb_recommends` feature with
pmbootstrap. The required patches have been merged to `pmbootstrap`'s `master`
branch, but are not in a released version yet.

So if you use a stable version of `pmbootstrap` to build a `generic-x86_64`
image, make sure to manually select the packages (e.g. via
`pmbootstrap install --add`).

Related: {{MR|4673|pmaports}}, {{patches|48226|pmbootstrap-devel}}
