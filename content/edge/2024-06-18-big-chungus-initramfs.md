title: "Boot images may no longer fit on some devices — workaround available"
date: 2024-06-18
---

Just now, {{MR|5000|pmaports}} was merged which brings many great improvements!
However, it also results in the postmarketOS boot image growing, which due an
unfortunate coincidence with a bug in boot-deploy, may result in partial boot
images being flashed to the boot image partition on devices with particularly
small boot image partitions.

If your device is affected by this, on system upgrade, you may get output
similar to this:

```
== Using boot-deploy to finalize/install files ==
==> Running hooks
==> kernel: device-tree blob operations
==> kernel: appending device-tree exynos4412-p4note-n8010
==> initramfs: creating boot.img
==> Checking free space at /boot
... OK!
==> Installing: /boot/initramfs
==> Installing: /boot/initramfs-extra
==> Installing: /boot/exynos4412-p4note-n8010.dtb
==> Installing: /boot/boot.img
==> Flashing boot image
Flashing boot.img to 'BOOT'
dd: error writing '/dev/mmcblk0p5': No space left on device
9+0 records in
8+0 records out
8388608 bytes (8.0MB) copied, 0.062911 seconds, 127.2MB/s
22:48:05.607882 boot-deploy completed in: 5.12s
```

In that case, you can (and should, as otherwise the device may no longer boot)
fix it by running `$ sudo apk add postmarketos-initramfs-minimal`, and then
`$ sudo apk fix` to get rid of the error.

If you feel like it, you can also either open an issue about your device being
affected (along with logs, ideally), or go the extra mile and submit a merge
request fixing the issue! To do so, you would find your device's device package
in the pmaports git repository, and add `postmarketos-initramfs-minimal` to
`depends`, and then increment `pkgrel` by 1 (so, if it for example was 3, you
should increase it to 4).
