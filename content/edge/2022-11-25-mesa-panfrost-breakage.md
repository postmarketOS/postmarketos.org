title: "Mesa 22.2.4 update broke various apps on rk3399 devices"
date: 2022-11-25
---

The latest Mesa update (22.2.4) broke some applications on devices that use the
panfrost driver, which includes the Pinephone Pro, Pinebook Pro, and RockPro64.
The regression causes them to crash with a wayland protocol error.

This was resolved with mesa 22.2.4-r1, so if you are affected just upgrade
your mesa again.

Also see:

- [pma#1791](https://gitlab.com/postmarketOS/pmaports/-/issues/1791)
- [aports!41417](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/41717)
- [mesa#7731](https://gitlab.freedesktop.org/mesa/mesa/-/issues/7731)
