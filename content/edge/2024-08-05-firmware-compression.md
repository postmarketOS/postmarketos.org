title: "Firmware compression may cause issues on devices using downstream kernels"
date: 2024-08-05
---

Firmware compression will soon be enabled in Alpine's linux-firmware package.
This will significantly reduce the amount of on-disk space the firmware uses
when installed. Notably, if you install every linux-firmware package Alpine
provides, the total storage space used will be halved from around 1 GB to around
0,5 GB.

All postmarketOS and Alpine Linux kernels version 5.2.0 or newer have been
adapted to work with this by setting `CONFIG_FW_LOADER_COMPRESS_ZSTD=y`, but
ones older than that do not have this option and as such cannot support firmware
compression.

This means that all devices using old downstream kernels (version 5.1 or
older) which get firmware from Alpine's linux-firmware package will have issues.
The exact issue will vary from device to device as it depends on what firmware
it gets from said package. Many devices use device- or SoC-specific firmware
packages. These are not affected as they will not start using firmware
compression as part of this. However, if you as a maintainer want to, you can
compress that firmware too to save some disk space! This has been implemented
for the PinePhone's ov5640 and rtl8723bs/rtl8723cs firmware since around half a
year ago.

So, in summary, these combinations will work:

 - Linux kernel version 5.2.0 or newer and using linux-firmware from Alpine
 - Linux kernel version 5.2.0 or newer and not using linux-firmware from Alpine
 - Linux kernel version 5.1 or older and not using linux-firmware from Alpine

And these combinations will not work any more:

 - Linux kernel version 5.1 or older and using linux-firmware from Alpine

If your device falls into the latter category, you can create a new firmware
package that includes uncompressed copies of whatever firmware your device
needs.

Related:

 - [MR compressing linux-firmware in Alpine](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/60282)
 - [MR compressing the PinePhone's firmware](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4812)
