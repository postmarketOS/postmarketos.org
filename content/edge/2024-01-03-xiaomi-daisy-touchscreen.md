title: "Linux 6.6.8: needs manual steps for Xiaomi MI A2 Lite"
date: 2024-01-03
---

With the upcoming kernel upgrade of linux-postmarketos-qcom-msm8953 to 6.6.8,
**goodix gt917d** is supported. The touchscreen selection was implemented in
lk2nd and both touchscreens are disabled by default.
For working touchscreen you need to upgrade msm8953-lk2nd manually.

It can be done in many ways:

* upgrade with fresh or upgraded build with pmbootstrap
```
pmbootstrap flasher flash_lk2nd
```
* download next prebuilt image from the site after 2023-12-29
[https://images.postmarketos.org/bpo/edge/xiaomi-daisy](https://images.postmarketos.org/bpo/edge/xiaomi-daisy)
```
unxz xiaomi-daisy-lk2nd.img.xz
fastboot flash boot xiaomi-daisy-lk2nd.img
```
* or you can build manually from
[https://github.com/msm8953-mainline/lk2nd](https://github.com/msm8953-mainline/lk2nd)


See the [lk2nd](https://wiki.postmarketos.org/wiki/Lk2nd/lk1st) and
[xiaomi-daisy](https://wiki.postmarketos.org/wiki/Xiaomi_Mi_A2_Lite_(xiaomi-daisy))
wiki articles for more information.

Related: {{MR|4671|pmaports}}
