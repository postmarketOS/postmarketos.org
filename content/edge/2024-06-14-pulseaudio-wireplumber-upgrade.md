title: "apk upgrade may fail when pulseaudio is installed"
date: 2024-06-14
---

Upgrading your system when the pulseaudio audio backend is installed may fail
due to a file conflict between `postmarketos-base-ui-audio-pulseaudio` and
`pulseaudio-wireplumber`. If this failure is ignored, audio may not work after
a reboot.

If this occurs, please run `apk fix` and confirm that the command executes
successfully. You may need to reboot to apply the updated configuration.

Also see:

- [No audio after upgrade bug report](https://gitlab.com/postmarketOS/pmaports/-/issues/2883)
- [MR moving wireplumber config to aports](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/66015)
- [MR dropping wireplumber config from pmaports](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/5225)
