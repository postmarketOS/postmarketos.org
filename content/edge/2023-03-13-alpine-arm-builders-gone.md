title: "Alpine's ARM builders are temporarily unavailable"
date: 2023-03-13
---

Alpine's ARM builders are currently unavailable. This means all new packages,
updates, downgrades, and other changes temporarily are delayed until Alpine
resolves this. This affects all ARM architectures available in Alpine, so armhf,
armv7, and aarch64. Other architectures are not affected. This also affects
stable releases.

Our postmarketOS repository is on the contrary not affected, so packages from
there will be updated as normal.

It is unknown when this will be resolved, but it should hopefully happen within
this week.

UPDATE: the arm builders are back!
