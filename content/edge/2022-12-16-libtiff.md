title: "libtiff related breakage in Alpine edge"
date: 2022-12-16
---

Libtiff had a soname bump, and now all packages in Alpine edge are being
rebuilt against it. This should be resolved soon, but as of writing there are
currently errors like:

<pre>
ERROR: unable to select packages:
  tiff-4.4.0-r1:
    conflicts: tiff-4.5.0-r1
    satisfies: ghostscript-10.0.0-r0[so:libtiff.so.5]
               gtk4.0-4.8.2-r1[so:libtiff.so.5]
               gdk-pixbuf-2.42.10-r0[so:libtiff.so.5]
               megapixels-1.6.0-r0[so:libtiff.so.5]
               libgxps-0.3.2-r1[so:libtiff.so.5]
               imagemagick-7.1.0.54-r0[so:libtiff.so.5]
               evince-libs-43.1-r0[so:libtiff.so.5]
               py3-pillow-9.3.0-r1[so:libtiff.so.5]
               postprocessd-0.2.1-r0[so:libtiff.so.5]
               spandsp-0.0.6-r2[so:libtiff.so.5]
  tiff-4.5.0-r1:
    conflicts: tiff-4.4.0-r1
    satisfies: poppler-22.12.0-r2[so:libtiff.so.6]
</pre>
