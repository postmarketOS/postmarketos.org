title: "GNOME SSH integration needs manual action to continue working"
date: 2024-05-11
---

With GNOME 46, the provider of the GNOME SSH integration was moved into its own
separate package. While new installations will have this installed
automatically, old installations will in most cases manually need to install
this package:

```
$ sudo apk add gcr-ssh-agent
```

This applies to Phosh, GNOME, and GNOME Mobile. Note that installing
gcr-ssh-agent automatically makes it the default SSH agent on your system.

Related MR: [aports!63893](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/63893)
