title: glGetString segmentation faults on ARMv7
date: 2022-08-31
---

This blog post is somewhat late. Roughly two months ago, the OpenGL function
glGetString started causing segmentation faults on ARMv7 devices using Mesa for
3D acceleration. Devices using Mesa for software rendering seem to be
unaffected. On affected devices, this results in programs like Xwayland and
X.Org segfaulting on startup, and Phosh showing the "Oh no" screen but
otherwise working fine outside of X11 apps being broken. Other Wayland
interfaces may or may not work depending on how they handle Xwayland startup.

This issue is being tracked at
[aports#14140](https://gitlab.alpinelinux.org/alpine/aports/-/issues/14140) and
[pmaports#1651](https://gitlab.com/postmarketOS/pmaports/-/issues/1651).
