title: "Flatpak broken with curl 8.10.0-r1"
date: 2024-09-13
---

As of curl-8.10.0-r1 from Alpine, certain Flatpak commands no longer work
properly. Versions curl-8.10.0-r0 and curl-8.9.1-r1 are known to work correctly
with Flatpak. For now, no workarounds or fixes are known aside from downgrading
curl.

This issue can be tracked in
[aports#16444](https://gitlab.alpinelinux.org/alpine/aports/-/issues/16444).
