title: "New postmarketOS installations now by default use doas instead of sudo"
date: 2024-09-11
---

New installations from now on will come with `doas` instead of `sudo`. This has
been a [long-standing](https://gitlab.com/postmarketOS/pmaports/-/issues/1121)
request, kindly implemented by [Aster](https://gitlab.com/JustSoup321). Since
`doas` is installed with `doas-sudo-shim`, you can continue using doas as sudo,
but please report if you encounter any issues.

If you encounter issues and want to switch back to `sudo`,
run the following command:

`doas apk add sudo !doas-sudo-shim`

Existing installations will continue using `sudo`. To switch to using `doas`,
run the following command:

`sudo apk add doas !sudo`
