title: "Upower 1.90.0 broke battery reporting on many devices"
date: 2022-09-04
---

The latest Upower upgrade at the time of writing (1.90.0) broke battery
reporting on many devices as it requires battery drivers to implement more
properties to be properly detected. If your device's battery is showing up as
being at 0% in your user interface and apps, you're likely affected by this. It
is known to affect various Snapdragon 845, Snapdragon 410, and NovaThor U5800
devices. Fixes for this are being worked on.

Related issue: [pma#1667](https://gitlab.com/postmarketOS/pmaports/-/issues/1677)
