title: "Short financial update and lookahead"
date: 2024-08-11
---

Since December 2023 postmarketOS has been collecting donations transparently
through [Open Collective](https://opencollective.com/postmarketos). Since
then, the postmarketOS team has also been working on strategies to spend the
collected donations in a meaningful and sustainable way, more on that soon in
a follow-up blog post. The blog post you are reading right now provides an
update on where we are standing financially after 8 months of collecting
donations.

## Financial status

Open Collective has helped us collect donations, with transparency to the public
about it. So, we have some useful graphs and information on top donations,
already – visible for everyone. However, most of that information includes
OpenCollective Europe fees, fees from the payment method (credit card, PayPal,
bank transfer), and donations to Open Collective. Therefore, we want to provide
a short update based on the raw
[transactions data](https://rest.opencollective.com/v2/postmarketos/transactions.csv)
from OpenCollective.

#### Income

From December 2023 to the end of July 2024 postmarketOS has received
€8538 in donations. Out of those, €7623 has been deposited in our bank
account. The rest (a bit less than 11%) has gone into the various fees
mentioned above.

We have identified that small transactions under €5 have an excessive amount of
payment processing fees. In some cases, we have seen up to 45% of fees for €1
transaction. We understand this might not be clear to people donating. To avoid
this, we have decided to raise the minimum donation amount to €5. This only
applies to future donors. If you have a recurring donation of such an amount,
we recommend you change the frequency of your donations. For example,
postmarketOS will receive more money from a donation of €10 once a year, than
from €1 per month, even though that should total up to €12 – due to said
transaction fees.

#### Expenses

In the same period (December 2023 to July 2024), we have spent a bit less than
€2000, more than half in the post-FOSDEM
[hackathon](https://postmarketos.org/blog/2024/02/14/fosdem-and-hackathon/)
that helped us set a project direction for many months. The rest has been
spent mostly on infrastructure, but also on stickers given out in conferences
and some HW for development.

#### Balance

In total, our savings have grown in more than €5500, while also covering
all the costs we had since we started collecting donations on OpenCollective.

For the last 6 months, we have steadily received between €500 and €600 in
recurring donations. This income outgrows our spending on infrastructure,
which is our only regular spending (we currently depend on a single
server, we will need more at some point) by much. This means that even in case
of unplanned emergency, we could lower any other spenditure and still be able
to provide a service without problems.

Overall, we see ourselves in a good situation. In terms of
building-an-operating-system-money it is of course not a huge pile, but
enough that we feel comfortable paying our first developer to work up
to 20h per week. More on that in a separate blog post soon!

A huge thanks to everybody who is making this possible! ❤️

If you appreciate the transparency of our finances and the work we are
doing with postmarketOS, consider
[joining our OpenCollective](https://opencollective.com/postmarketos)!
