title: "postmarketOS selected for\nNGI Zero Core funding / The European Union must keep funding free software"
date: 2024-07-17
---
## Introduction

We have applied for a grant from [NLnet's](https://nlnet.nl)
[NGI Zero Core](https://nlnet.nl/core/) fund and it
[has been accepted](https://nlnet.nl/project/postmarketOS-daemons/)! It will
allow us to spend significant additional hours on improving support for
[PipeWire](https://gitlab.com/groups/postmarketOS/-/milestones/15),
[iwd](https://gitlab.com/groups/postmarketOS/-/milestones/20),
[systemd](https://gitlab.com/groups/postmarketOS/-/milestones/22),
a proof of concept for
[immutable rootfs](https://gitlab.com/groups/postmarketOS/-/milestones/25)
and the next two postmarketOS releases (v24.12 and v25.06). All with the
overall goal of making postmarketOS more reliable and usable for non-technical
users. We are very grateful and eager to get started!

We originally planned to make this announcement together with the next monthly
blog post, which will come in a few days. But unfortunately we have learned
that the European Commission did not mention the Next Generation Internet
programmes anymore in their draft of funding programmes for 2025. We are
deeply saddened about this, because NGI and NLnet have been funding
[over a thousand free and open source software projects](https://nlnet.nl/project/index.html)
and we consider it critically that this will also be done in the future.

A small but very important part of that is what they have done for Linux
Mobile. NGI invested in projects from
[postmarketOS](https://nlnet.nl/project/postmarketOS/)
[multiple](https://nlnet.nl/project/pmOS-23-24/)
[times](https://nlnet.nl/project/MobileSettings/),
[Cell broadcast support for the Linux Mobile Stack](https://nlnet.nl/project/SMS-CB/),
[Maemo Leste](https://nlnet.nl/project/MaemoLeste/),
[Mobile NixOS](https://nlnet.nl/project/mobile-nixos/),
[Mepo](https://nlnet.nl/project/Mepo/)
and
[Replicant on Pinephone 1.2](https://nlnet.nl/project/Replicant-Pinephone/)
just to name a few.

In other words, NGI has made a dramatic impact to improve our sustainable,
privacy and attention respecting Linux Mobile ecosystem. One of the very few
answers we have to the duopoly of Android and iOS from Silicon Valley.

We urge the European Commission to reconsider.

*The following is an open
letter initially published in French by the
[Petites Singularités](https://ps.zoethical.org/pub/lettre-publique-aux-ncp-au-sujet-de-ngi/)
association. To co-sign it, please publish it on your website in your preferred
language, then add yourself to [this table](https://pad.public.cat/lettre-NCP-NGI).*

## The European Union must keep funding free software

Open Letter to the European Commission.

Since 2020, Next Generation Internet ([NGI](https://www.ngi.eu)) programmes,
part of European Commission's Horizon programme, fund free software in Europe
using a cascade funding mechanism (see for example NLnet's
[calls](https://www.nlnet.nl/commonsfund)). This year, according to the Horizon
Europe working draft detailing funding programmes for 2025, we notice that
Next Generation Internet is not mentioned any more as part of Cluster&nbsp;4.

NGI programmes have shown their strength and importance to supporting the
European software infrastructure, as a generic funding instrument to fund
digital commons and ensure their long-term sustainability. We find this
transformation incomprehensible, moreover when NGI has proven efficient and
economical to support free software as a whole, from the smallest to the most
established initiatives. This ecosystem diversity backs the strength of
European technological innovation, and maintaining the NGI initiative to
provide structural support to software projects at the heart of worldwide
innovation is key to enforce the sovereignty of a European infrastructure.
Contrary to common perception, technical innovations often originate from
European rather than North American programming communities, and are mostly
initiated by small-scaled organizations.

Previous Cluster 4 allocated 27 million euros to:

- "Human centric Internet aligned with values and principles commonly shared in
  Europe" ;
- "A flourishing internet, based on common building blocks created within NGI,
  that enables better control of our digital life" ;
- "A structured ecosystem of talented contributors driving the creation of new
  internet commons and the evolution of existing internet commons".

In the name of these challenges, more than 500 projects received NGI funding in
the first 5 years, backed by 18 organisations managing these European funding
consortia.

NGI contributes to a vast ecosystem, as most of its budget is allocated to fund
third parties by the means of open calls, to structure commons that cover the
whole Internet scope - from hardware to application, operating systems, digital
identities or data traffic supervision. This third-party funding is not renewed
in the current program, leaving many projects short on resources for research
and innovation in Europe.

Moreover, NGI allows exchanges and collaborations across all the Euro zone
countries as well as "widening countries" [^1], currently both a success and an
ongoing progress, likewise the Erasmus programme before us. NGI also
contributes to opening and supporting longer relationships than strict project
funding does. It encourages implementing projects funded as pilots, backing
collaboration, identification and reuse of common elements across projects,
interoperability in identification systems and beyond, and setting up
development models that mix diverse scales and types of European funding
schemes.

While the USA, China or Russia deploy huge public and private resources to
develop software and infrastructure that massively capture private consumer
data, the EU can't afford this renunciation.
Free and open source software, as supported by NGI since 2020, is by design the
opposite of potential vectors for foreign interference. It lets us keep our
data local and favors a community-wide economy and know-how, while allowing an
international collaboration.
This is all the more essential in the current geopolitical context: the
challenge of technological sovereignty is central, and free software allows
addressing it while acting for peace and sovereignty in the digital world as a
whole.

[^1]: As defined by Horizon Europe, widening Member States are Bulgaria,
Croatia, Cyprus, Czechia, Estonia, Greece, Hungary, Latvia, Lituania, Malta,
Poland, Portugal, Romania, Slovakia, and Slovenia. Widening associated
countries (under condition of an association agreement) include Albania,
Armenia, Bosnia, Feroe Islands, Georgia, Kosovo, Moldavia, Montenegro, Morocco,
North Macedonia, Serbia, Tunisia, Turkeye, and Ukraine. Widening overseas
regions are Guadeloupe, French Guyana, Martinique, Reunion Island, Mayotte,
Saint-Martin, The Azores, Madeira, the Canary Islands.
