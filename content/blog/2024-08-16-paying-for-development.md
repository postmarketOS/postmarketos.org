title: "Paying for development from the donations for the first time!"
date: 2024-08-16
---
As mentioned in
[our previous blog post](/blog/2024/08/11/pmOS-financial-update/),
thanks to your donations we do not only have enough to pay the server bills.
But we can also pay somebody to work part-time on postmarketOS from the
donations, which will be a first since we moved to
[OpenCollective](https://opencollective.com/postmarketOS)!

## Jane: systemd, musl, and upstreaming

For starters we have decided to pay for upstreaming patches to improve the
compatibility between musl and systemd. We have agreed to pay Jane, one of the
postmarketOS Trusted Contributors to do this. For the sake of transparency, the
agreement is as follows:

* Jane will freelance as a student (max. 20 hours a week).
* Jane will bill for work related to:
    * Figuring out and documenting which patches in the
      [postmarketOS systemd](https://gitlab.com/postmarketOS/systemd) repo are
      upstreamable, and which aren't.
    * Getting patches into upstreamable state, pushing PRs, discussing with
      maintainers, addressing the feedback, and repeat.
    * Learning systemd internals, code style, and whatever is needed to be able
      to push those patches.
    * If some patches have a fit in musl, preparing them, sending them to the
      mailing list, discussing with maintainers, addressing the feedback, and
      repeat.
* We have set a maximum limit on expense close to the money we have saved from
  the beginning of the year. That is certainly flexible depending on how
  well this goes for both parties.
* Jane will keep the community and the team updated on the work that gets done.

This is the first time we go into an economic agreement, and the first time we
are going to fund development. We understand there will be lots of lessons to
be learned, so of these terms might change as the work gets done.

### Why fund this project and not (insert my favorite project that needs attention)?

postmarketOS has very limited funds, and this is a hard decision. However, the
team has identified the lack of compatibility between musl and systemd as one of
the critical points to improve the future stability of postmarketOS.

In addition, we believe that money is best spent on a project that would otherwise
be very unlikely to be done by volunteers. We deeply appreciate everybody in our
community doing volunteer work, including team members like Clayton and Pablo
working full-time or part-time without more support than external grants.
However, volunteer work cannot reach everywhere. While some projects
provide lots of non-economic rewards to people working on them, others don't.
Improving the compatibility between systemd and musl is within
the second group, and thus why we believe it's important to fund it.

### Why Jane?

Jane is a postmarketOS Trusted Contributor, she was the first person that
managed to boot a postmarketOS device with systemd as proof-of-concept (back in
June 2023), she has been contributing to the systemd work in postmarketOS,
and rebased the set of Open Embedded patches on top of new releases several
times. Within our project, she is certainly the most appropriate person to do
this work.

She is not a musl or systemd contributor, and she will have to continue learning
the conventions of those communities. However, we don't think that this will be
a big hurdle. We think that paying for one of our contributors who is already
very familiar with postmarketOS and understands exactly what we need, to learn
about those communities is a smart decision. Both short-term and long-term.

*If you appreciate the work we're doing on postmarketOS, consider
[joining our OpenCollective](https://opencollective.com/postmarketos).*
