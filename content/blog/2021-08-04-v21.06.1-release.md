title: "postmarketOS Release: v21.06 Service Pack 1"
date: 2021-08-04
preview: "2021-08/v21.06.1_pinephone_sxmo.jpg"
---
[#grid side#]
[![](/static/img/2021-08/v21.06.1_pinephone_sxmo_thumb.jpg){: class="wfull border" }](/static/img/2021-08/v21.06.1_pinephone_sxmo.jpg)
[#grid text#]

The first service pack for the current stable branch has been released, v21.06.1.


Service packs bring improvements from the edge channel of postmarketOS to the
stable release after they have been thoroughly tested.

Changes:

* Sxmo upgrade to 1.5.0
* postmarketOS Tweaks upgrade to 0.7.3
* Firefox Mobile Config upgrade to 2.2.0
* Pinephone kernel upgrade to 5.12.12
* Added Bluetooth MPRIS support for media keys
* GNOME Clocks can now wake the Pinephone from suspend when alarms trigger
* All kernels/devices in the stable release support nftables
[#grid end#]

For best stability, the service pack contains tried and tested versions of applications and kernels. 

Find the most recent images at our [download page](/download/).

Existing users of the v21.06 release will receive the service pack changes on
their next system update.

## Comments

* [Mastodon](https://fosstodon.org/@postmarketOS/106708493195993201)
* [Lemmy](https://lemmy.ml/post/76445)
<small>
* [HN](https://news.ycombinator.com/item?id=28085449)
</small>
