title: "v21.12 Service Pack 5"
date: 2022-05-15
preview: "2022-05/pinephone_tweaks_sxmo.jpg"
---

[#grid side#]
[![](/static/img/2022-05/pinephone_tweaks_sxmo.jpg){: class="w300 border"}](/static/img/2022-05/pinephone_tweaks_sxmo.jpg)
[#grid text#]

As we are getting to the end of the v21.12 lifecycle with v22.06 on the
horizon, we have one more small service pack to announce.

## Contents

* [postmarketOS Tweaks 0.12](https://gitlab.com/postmarketOS/postmarketos-tweaks/-/tags/0.12.0) adds support for changing
  the color scheme in Sxmo DWM.
  
* [SDM845](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_845_(SDM845)) Linux kernel upgrade to
  [5.17.5](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3118).
  
* [MSM8996](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_820/821_(MSM8996)) Linux kernel upgrade to
  [5.17.6](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3126).
  
* Fix issue with the wifi not being detected on boot on the samsung-espresso3g
  ([pma#1211](https://gitlab.com/postmarketOS/pmaports/-/issues/1211)).

[#grid end#]

## How to get it

Find the most recent images at our [download page](/download/). Existing users of the v21.12 release will receive this
service pack automatically on their next system update. If you read this blog post right after it was published, it may
take a bit until binary packages are available.

Thanks to everybody who made this possible, especially our amazing community members and upstream projects.
