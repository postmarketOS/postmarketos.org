title: "Considering SourceHut, Part 2"
date: 2022-08-26
preview: "2022-07/sourcehut.jpg"
---

[![](/static/img/2022-07/sourcehut_thumb.jpg){: class="wfull border" }](/static/img/2022-07/sourcehut.jpg)

We recently added a post, [Considering SourceHut](/blog/2022/07/25/considering-sourcehut/),
where we explained our desire to move from our current source code and issue
hosting on gitlab.com to SourceHut. Along with a tentative rollout plan and a
call out to postmarketOS contributors to give their feedback in an issue
created specifically for that purpose
([postmarketos#49](https://gitlab.com/postmarketOS/postmarketos/-/issues/49)). 
After receiving a lot of thoughtful feedback (and yes we did read them all!)
from contributors and community members who read the post, we felt is was
necessary to provide an update.

So is moving to SourceHut the best decision for postmarketOS? We don't know.
Some of the feedback we received was supportive of the move to SourceHut, some
was against it, and others showed up to advocate for moving to other hosting
solutions.

What we do know is, now would be the best time to *try* it.

Acting now would allow us to have a slow and safe roll out plan where we can
get some real experience with having part of pmOS on SourceHut, without
disrupting the workflow of most contributors. All the while, we would still
have the option to revisit and, if necessary, roll back any changes at the end
of this testing phase.

After another postmarketOS core team meeting to discuss this topic, we decided
that this is what we will do. We'll move pmbootstrap.git (again, only the git
repository, not the issues!) to SourceHut soon. In January of 2023, we will
review how well that worked or not, and based on that continue with the plan or
revert and do something else. We are looking forward to seeing first patches
sent in!

If you haven't read the post yet
([Considering SourceHut](/blog/2022/07/25/considering-sourcehut/)), or the
comments on the issue we created for gathering feedback
([postmarketos#49](https://gitlab.com/postmarketOS/postmarketos/-/issues/49)), 
then we invite you to do so since they provide a lot more context.
