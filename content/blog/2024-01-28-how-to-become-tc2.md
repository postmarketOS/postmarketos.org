title: "How to become a Trusted Contributor, part 2"
date: 2024-01-28
---

After our
[How to become a Trusted Contributor](https://postmarketos.org/blog/2023/12/03/how-to-become-tc/)
blog post was received with great enthusiasm by the community (and already have
two new TCs since, shout out to @travmurav and @f_), we thought that it would
be a great idea to expand its scope. We know that people have been contributing
to postmarketOS in many ways, not just via code reviews and submitting patches.
So if you have been contributing to postmarketOS for the past six months or
longer and don't fancy yourself a developer, there is now a path for you to
become a Trusted Contributor too!

Some examples of what types of contributions can qualify you as a Trusted
Contributor include:

- making [accessibility](https://wiki.postmarketos.org/wiki/Accessibility) improvements
- supporting others in our chat rooms
- supporting others in our issue trackers
- creating artwork or other promotional materials
- writing blog posts for postmarketOS.org (the main and/or "edge" blogs)
- podcast / audio editing

If you're curious how to get started with any of those things today, in hopes
of applying to be a TC later, please do not hesitate to
[reach out to us in chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC)!
If you find other tasks that we didn't think of, which would also have a good
impact on the project or our community, feel free to reach out as well.

Trusted Contributors enjoy street cred (public contributions to a large FOSS
project can look good on a resume!) and their own `@postmarketos.org` email
address.

The process to apply is largely the same as
[what we detailed previously](https://postmarketos.org/blog/2023/12/03/how-to-become-tc/#the-new-process):
file [a new confidential issue](https://gitlab.com/postmarketOS/apply-for-trusted-contributor/-/issues/new)
in our application repo, find existing team members to endorse you, and list
out your contributions and achievements to support your application.

We also got a bit of behind-the-scenes audio for you in
[the podcast](https://cast.postmarketos.org/episode/37-Clayton-goes-full-time-v23.12-smart-speakers-musl-locales/).
That's it, thanks for reading this blog post!
