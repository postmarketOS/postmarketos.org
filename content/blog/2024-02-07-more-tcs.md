title: "postmarketOS in 2024-02: More trusted contributors"
date: 2024-02-07
---

It's February, and we are still sticking to our new year resolutions and
greeting the month with a blog post update! Among other topics, this month
has been crazy busy with infrastructure migrations (you might have noticed
the performance and reliability improvements with the postmarketOS server!).
And of course FOSDEM was a few days ago! We have stayed in Belgium and are
half-way through a 3-day hackathon with most of the team. It is very exciting
to be able to meet in person, and we are getting lots of work and long-term
planning done. We have financed part of this through
your [donations](https://opencollective.com/postmarketos), so they are already
making an impact, thanks to everybody! We will make sure to write a summary
blog post for FOSDEM and the Hackathon once it is all over!

## Governance: more ways to become a Trusted Contributor

Through the last months, we have been working on improving the governance
structure of the project. And this month we followed up by
[refining the process](https://postmarketos.org/blog/2024/01/28/how-to-become-tc2/)
to become a Trusted Contributor. Now, you don't need to be a developer to
become official part of the postmarketOS team! There are plenty of ways to
contribute to the project that are not just with code. We want to encourage
people to do so, and give them credit for what they do!

And seems like the Governance improvements are actually having an impact on
the project. Since last update, we have welcomed two new Trusted Contributors:
[Ferass El Hafidi](https://gitlab.com/funderscore) and
[Arnav Singh](https://gitlab.com/Arnavion)! They have both been doing great
work around different parts of the project, and we are very happy to have them
on-board! Maybe next month we can introduce our first non-developer of the team?

## So what's new?
* After a short break, we released a new
  [podcast episode](https://cast.postmarketos.org/episode/37-Clayton-goes-full-time-v23.12-smart-speakers-musl-locales/), and recorded another at FOSDEM!
* We moved pmbootstrap back to
  [gitlab.com](https://postmarketos.org/blog/2024/01/17/moving-pmbootstrap/).
* Work on generic Intel and AMD devices. There has been lots of work put to
  rework and improve the packaging and hardware support on first install.
  Thanks Clayton!
* The Pinebook Pro has a
  [new maintainer](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4736)!
  There was a call to move it out of community due to the lack of maintenance,
  and [Quade Curry](https://gitlab.com/QC8086) stepped up. Thanks a lot!
* Work on the new
  [kernel config fragments](https://gitlab.com/groups/postmarketOS/-/epics/2)
  implementation has continued. Thanks Jenneron, Clayton, and Arnav!
* We have 4 new supported devices: Samsung J6, OnePlus 9 Pro, Samsung Galaxy
  Tab 4 7.0 Wi-Fi, and Amazon Fire TV Stick Lite. Also, 2 other devices gained
  mainline support: OnePlus 9, Motorola Moto G 4G.
* Dozens of kernels were updated to the latest linux 6.7 release. We were able
  to demo some of them at FOSDEM, and got great feedback about the fact that
  all our devices are running the latest kernel. Thanks to all of our kernel
  contributors!
* Work on merging some of our low-level components into
  [BuffyBox](https://gitlab.com/postmarketOS/buffybox) has started! Thanks
  [cherrypicker](https://gitlab.com/cherrypicker)!
* We merged some good improvements and generalization for
  [exynos5420](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4721)
  devices. Thanks [Grimler](https://gitlab.com/Grimler)!
* mkinitfs and boot-deploy got some cleanups and improvements related to
  EFI booting and the deprecated osk-sdl. Thanks Caleb and Clayton!
* The Fairphone 5, the newest device we support, got some further
  [improvements](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4761).
  Thanks Luca!
* Mate and LXQT got some small improvements. Thanks
  [Jakko](https://gitlab.com/jakko)!
* btrfs installations do now get a much better
  [default experience](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2233)
  on first install. Thanks [papiris](https://gitlab.com/papiris)!
* We got an artist design
  [wallpapers](https://gitlab.com/postmarketOS/artwork/-/merge_requests/25)
  specific for postmarketOS. They are not integrated yet, but we're
  [working on it](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4716).
* We moved from self-hosting our email to [Migadu](https://www.migadu.com).
  This was enabled by your donations, as we know that we can pay for it long-
  term, and frees up some of our time to do more exciting postmarketOS stuff!
* The Mastodon [pmOS devices bot](https://mastodon.social/@pmOS_devices) is up
  and running again. Make sure to follow it if you're interested in the new
  devices that keep coming. Thanks Raffaele!
* A new
  [pmbootstrap release](https://gitlab.com/postmarketOS/pmbootstrap/-/tags/2.2.0)
  is out. It contains many fixes and improvements, including some relevant
  performance improvements during build, several envkernel changes, and much
  more. Thanks Ollie and all the contributors!

## And what's next?

We of course have great dreams and big plans coming out of the hackathon.
Short-term plans are not so ambitions, but still exciting!

* We hope to ship the new
  [wallpapers](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4716)
  soon.
* The work on the kconfig fragments will certainly continue!
* We will switch the latest GTK release, with
  [new renders](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4773).
* We will
  [stop supporting](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4732)
  osk-sdl at a close point in time.
* We will improve the way we handle the recommended packages for
  [GNOME](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4756) and
  [KDE Plasma](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4798), so
  there is a more similar experience in-between different UIs of the same
  desktop.

## Conclusion

Last month's blog post seems to have been well received, so we hope to continue
with this series! In general, this month there seems to have been a bit less
throughput in terms of features, but not every month there's a member starting
to work on the project full-time! Also, we have all been preparing for FOSDEM,
and the output from FOSDEM and the hackathon will hopefully make a big impact.
So we look forward to continue the work and keep you updated!

If you appreciate the work we're doing on postmarketOS, and want to support us,
consider [joining our OpenCollective](https://opencollective.com/postmarketos).
