title: "postmarketOS in 2024-03: systemd and more trusted contributors"
date: 2024-03-13
---

It's mid-March, and here comes a new update on what has happened in
postmarketOS for the last month! For the Core Team, this month has been
characterized for the classic post-FOSDEM hangover: a mix between a huge
amount of excitement, and the realization that real life exists, and not every
weekend there's FOSDEM. As you hopefully all know, part of that excitement has
been the final touches for making systemd changes public. If you have not
read [the systemd announcement](https://postmarketos.org/blog/2024/03/05/adding-systemd/)
yet, please make sure to do so. But in short, systemd gets added for Plasma
and GNOME based UIs and will be the default there in pre-built images,
and we put in a lot of work to make sure that Sxmo can still use OpenRC
and it is even possible to build your own Plasma and GNOME based images with
OpenRC if you prefer. However, it's also been an exciting month for other
reasons. Since we started writing the last post, there has been more than
150 MRs merged in the
[postmarketOS namespace](https://gitlab.com/groups/postmarketOS), and this
seems to be a consistent upwards trend!

## Scaling up: thoughts and improvements

The last month has been incredibly busy for the Core Team and the Trusted
Contributors. It has been most likely
the month with the most MRs opened and merged in a while if not ever.
This means increased work for everybody, while the rest of the ecosystem
continues moving forward, as for example with the KDE
[Plasma 6](https://kde.org/announcements/megarelease/6/) release! Reviewing all
these MRs is not an easy task, and sometimes the list of pending MRs and opened
issues grows, and we can take a bit to get to your contributions. This would have
been much harder without the Trusted Contributors program, but we certainly have
room for improvement. If you have ideas of things that we could be doing better,
or how to improve our technical and social processes, do not hesitate to reach out
in [chat](https://wiki.postmarketos.org/wiki/Matrix_and_IRC) or at
[mastodon](https://fosstodon.org/@postmarketOS).

## Looking for Plasma maintainers

The upgrade to Plasma 6 has been a huge amount of effort, and
[Bart](https://gitlab.com/PureTryOut) more or less completely did it by himself.
If you are a happy Plasma user in postmarketOS or Alpine and would not only like
to see it become better, but actively take part in that, then consider becoming
a Plasma maintainer in Alpine/postmarketOS! You can reach out to us via IRC and
matrix in the postmarketos-devel channel, and tag `@PureTryOut`. Maintainers
don't need to be able to do everything, every small bit of work that could be
taken from the current only maintainer would already be a huge help.

## So what's new?

* We have 10 new device ports: Google Asurada, Corsola, and Cherry
  Chromebooks; Samsung Galaxy J5 (2017), J7 Pro, and A32 4G; Xiaomi Mi 9T /
  Redmi K20 and Redmi 12 and Note 9; and Clockwork Tech ClockworkPi uConsole CM4.
  This has been a very successful month in terms of device ports, thanks to all
  our contributors!
* [Robert Eckelmann](https://gitlab.com/longnoserob) became a Trusted
  Contributor! He is the first non-developer to join, and comes with great ideas
  and contributions. Welcome to the team!
* We added
  [more options](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4795)
  for community devices, so there is better support for peripherals and other
  features. Thanks Luca!
* We extended our custom
  [recommends logic](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2238),
  and now use it
  [GNOME](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4756) and
  [KDE Plasma](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4798) UIs.
  This warranties a more consistent experience through different UI. Thanks
  Clayton and Bart! (This was already shipped as part of the pmbootstrap 2.2.x
  releases, but not explicitly mentioned in the 2024-02 update blog post.)
* Finally [drop](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4732)
  osk-sdl as advertised. Thanks Clayton and [David](https://gitlab.com/okias)!
* Remove unused screen deviceinfo variables
  [from the template](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2253).
  Thanks [Andras](https://gitlab.com/andrisas)!
* Started work on using zstd-compressed firmware for some kernels
  ({{issue|39|postmarketos-mkinitfs}}). Lots of people from the team
  worked together on this, as well as with Alpine in order to reduce the
  size of shipped firmware. Thanks Newbyte, Clayton, f_, and Arnav!
* Multiple kernels were updated to the newest LTS version (6.6), and others
  continued to be updated to the latest stable release, 6.7. Thanks a lot to all
  our kernel maintainers!
* Continue the work on wallpapers, with
  [some fixes](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4810)
  to the N900 and more new
  [artwork](https://gitlab.com/postmarketOS/artwork/-/merge_requests/31)
  available. Thanks [Sicelo](https://gitlab.com/sicelo) and
  [dikasp](https://gitlab.com/dikasetyaprayogi)!
* Port
  [msm8916 devices](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4826)
  to use extlinux. Thanks Nikita!
* Start integrating [Mypy](https://www.mypy-lang.org/) into CI of multiple of
  our tools, and bump pmbootstrap's minimum required Python version to 3.9
  ({{MR|2265|pmbootstrap}}). Thanks Newbyte and Pablo!
* Install non-free firmware
  [by default](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2255),
  see the related [edge blog post](/edge/2024/02/15/default-nonfree-fw/)
  for reasoning. In the process, also add
  [more firmware](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4794)
  in generic x86_64 packages. Thanks Clayton!
* Start offering to build images for a few
  [testing devices](https://wiki.postmarketos.org/wiki/Device_categorization#Official_Images)
  under some conditions.
  In consequence, devices don't need to be moved to community to get pre-built
  images. Thanks Oliver!
* All remaining infrastructure has been moved from the old and less reliable
  server to the new server, and the subscription for the old server has been
  canceled (so we don't need to pay for it anymore). Thanks Luca!

## And what's next?

* [GNOME 46](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/61987)
  coming to alpine in some weeks.
* Continue our path towards
  [systemd](https://gitlab.com/postmarketOS/pmaports/-/issues/2632) integration
  with KDE Plasma and GNOME.
* We plan to do some changes to the devices categorization, to help device
  maintainers, and ensure consistent quality among devices of the same category.
* More workforce will be available to join the team. Stay posted!

## Conclusion

We are very happy with what we got done this month together, and that the systemd
news were mostly received positively. There even was a
[poll on Mastodon](https://fosstodon.org/@martijnbraam/112050130609050413) with 539
people, of which 72% voted for systemd. We hope to keep bringing great news, while
serving our community and collaborating closely with other projects.

If you appreciate the work we're doing on postmarketOS, and want to support us,
consider [joining our OpenCollective](https://opencollective.com/postmarketos).
